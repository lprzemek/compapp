package com.przemekdev.compass;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

public class Compass implements SensorEventListener {

    private AngleLowPassFilter angleLowPassFilter;
    private SensorManager sensorManager;
    private Sensor accelerometer;
    private Sensor magnetometer;
    float[] gravityValues;
    float[] geomagneticValues;
    float[] rotationMatrix = new float[9];
    float[] inclinationMatrix = new float[9];
    float[] orientationValues = new float[3];
    float azimuthInRadians;
    float azimuthInDegrees;
    boolean success;
    public CompassListener compassListener;

    public void setCompassListener(CompassListener compassListener) {
        this.compassListener = compassListener;
    }

    public Compass(Context context) {
        sensorManager = (SensorManager)context.getSystemService(Context.SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometer = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        angleLowPassFilter = new AngleLowPassFilter();

    }

    public void start(){
        sensorManager.registerListener(this,accelerometer,SensorManager.SENSOR_DELAY_UI);
        sensorManager.registerListener(this, magnetometer, SensorManager.SENSOR_DELAY_UI);
    }

    public void stop () {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            gravityValues = sensorEvent.values;
        }
        if(sensorEvent.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
            geomagneticValues = sensorEvent.values;
        }
        if(gravityValues != null && geomagneticValues != null){
            success = SensorManager.getRotationMatrix(rotationMatrix,inclinationMatrix,gravityValues,geomagneticValues);
            if(success){
                SensorManager.getOrientation(rotationMatrix, orientationValues);
                azimuthInRadians = orientationValues[0];
                angleLowPassFilter.add(azimuthInRadians);
                azimuthInRadians = angleLowPassFilter.average();
                azimuthInDegrees = (float) Math.toDegrees(azimuthInRadians);
                compassListener.onCompassAzimuthChanged(azimuthInDegrees);
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }
}
