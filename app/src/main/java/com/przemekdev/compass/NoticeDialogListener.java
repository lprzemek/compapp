package com.przemekdev.compass;

public interface NoticeDialogListener {
    public void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog);
    public void onDialogNegativeClick(android.support.v4.app.DialogFragment dialog);
}
