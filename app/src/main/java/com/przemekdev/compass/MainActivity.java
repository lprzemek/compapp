package com.przemekdev.compass;

import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import com.przemekdev.compass.utils.AzimuthTransformer;

import java.text.NumberFormat;
import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements NoticeDialogListener {

    private static final String ERR = "error";

    @Bind(R.id.coordinates_btn)
    protected Button coordinates_btn;
    @Bind(R.id.compass_iv)
    protected ImageView compass_iv;
    @Bind(R.id.header_tv)
    protected TextView header_tv;
    @Bind(R.id.arrow_iv)
    protected ImageView destinationArrow_iv;

    private EditText latitude_et;
    private EditText longitude_et;

    private Compass compass;
    private CoordinatesDialog coordinatesDialog;
    private Animation compassAnimation;
    private float currentAzimuth;

    private Location destinationLocation;
    float bearing;
    private NumberFormat formatter;
    private Animation arrowAnimation;
    private float currentArrowAzimuth;
    boolean requestingLocationUpdates;
    private LocationClient locationClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setupNumberFormatter();
        requestingLocationUpdates=false;
        coordinatesDialog = new CoordinatesDialog();
        setupCompassListener();
        setupCoordinatesButtonListener();
        locationClient = new LocationClient(this);
    }

    private void setupNumberFormatter(){
        formatter = NumberFormat.getInstance();
        formatter.setMaximumFractionDigits(0);
    }

    private void setupCompassListener() {
        compass = new Compass(this);
        compass.setCompassListener(new CompassListener() {
            @Override
            public void onCompassAzimuthChanged(float azimuth) {
                if (compass_iv == null) {
                    return;
                }
                if (azimuth < 0) {
                    String s = formatter.format((azimuth + 360));
                    header_tv.setText(s + AzimuthTransformer.transformDirectionToAlphanumeric(azimuth));
                } else {
                    String s = formatter.format(azimuth);
                    header_tv.setText(s + AzimuthTransformer.transformDirectionToAlphanumeric(azimuth));
                }
                compassAnimation = new RotateAnimation(-currentAzimuth, -azimuth,
                        Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF,
                        0.5f);
                currentAzimuth = azimuth;
                compassAnimation.setDuration(500);
                compassAnimation.setRepeatCount(0);
                compassAnimation.setFillAfter(true);
                compass_iv.startAnimation(compassAnimation);
                if (requestingLocationUpdates) {
                    updateBearing();
                    animateDestinationArrow(bearing);
                }
            }
        });
    }

    private void updateBearing(){
        bearing = locationClient.getCurrentLocation().bearingTo(destinationLocation);
    }


    private void setupCoordinatesButtonListener() {
        coordinates_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                coordinatesDialog.show(getSupportFragmentManager(), "");
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        compass.start();
        locationClient.reconnect();
    }

    @Override
    protected void onPause() {
        super.onPause();
        compass.stop();
    }


    @Override
    protected void onStart() {
        super.onStart();
        locationClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        locationClient.disconnect();
    }

    @Override
    public void onDialogPositiveClick(android.support.v4.app.DialogFragment dialog) {
        latitude_et = (EditText) dialog.getDialog().findViewById(R.id.latitude_et);
        longitude_et = (EditText) dialog.getDialog().findViewById(R.id.longitude_et);
        String latitude = latitude_et.getText().toString();
        String longitude = longitude_et.getText().toString();
        double lat1;
        double lon1;
        try {
            lat1 = Double.parseDouble(latitude);
            lon1 = Double.parseDouble(longitude);
            destinationLocation = new Location("");
            destinationLocation.setLatitude(lat1);
            destinationLocation.setLongitude(lon1);
            bearing = locationClient.setInitialDestinationBearing(destinationLocation);
            requestingLocationUpdates = true;
        } catch (NumberFormatException e){
            Log.e("ERR","Error while parsing latitude || longitude");
        }
    }

    @Override
    public void onDialogNegativeClick(android.support.v4.app.DialogFragment dialog) {
    }

    private void animateDestinationArrow(float bearing) {
        float positiveDestinationBearing = getPositiveDestinationBearing();
        float finalBearing = positiveDestinationBearing + bearing;
        showDestinationArrow();
        arrowAnimation = new RotateAnimation(currentArrowAzimuth,finalBearing,Animation.RELATIVE_TO_SELF,0.5f,
                Animation.RELATIVE_TO_SELF,0.5f);
        currentArrowAzimuth = finalBearing;
        arrowAnimation.setDuration(500);
        arrowAnimation.setRepeatCount(0);
        arrowAnimation.setFillAfter(true);
        destinationArrow_iv.startAnimation(arrowAnimation);
    }

    private float getPositiveDestinationBearing() {
        float currentNorthAzimut;
        if(currentAzimuth < 0) {
            currentNorthAzimut = currentAzimuth + 360;
        } else {
            currentNorthAzimut = currentAzimuth;
        }
        return 360 - currentNorthAzimut;
    }

    private void showDestinationArrow() {
        destinationArrow_iv.setVisibility(View.VISIBLE);
    }
}


