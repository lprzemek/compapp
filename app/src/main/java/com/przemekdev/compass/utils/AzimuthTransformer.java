package com.przemekdev.compass.utils;


public class AzimuthTransformer {

    public static String transformDirectionToAlphanumeric(float azimuth){

        String result = null;

        if(azimuth > 0 && azimuth < 90){
            result=" NE";
        } else if(azimuth > 90 && azimuth < 180){
            result=" SE";
        } else if(azimuth > -90 && azimuth < 0){
            result=" NW";
        } else if(azimuth > -180 && azimuth < -90){
            result=" SW";
        } else if(azimuth == 0){
            result=" N";
        } else if(azimuth == 90){
            result=" E";
        } else if(azimuth == 180 || azimuth == -180){
            result=" S";
        } else if(azimuth == -90){
            result=" W";
        }
        return result;
    }
}
