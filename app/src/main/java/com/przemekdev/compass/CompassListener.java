package com.przemekdev.compass;

public interface CompassListener {

     void onCompassAzimuthChanged(float azimuth);
}
