package com.przemekdev.compass;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.widget.Button;

public class CoordinatesDialog extends android.support.v4.app.DialogFragment {

    NoticeDialogListener noticeDialogListener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            noticeDialogListener = (NoticeDialogListener) activity;
        } catch (ClassCastException e){
            throw new ClassCastException(activity.toString());
        }
    }
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();


        dialogBuilder.setView(inflater.inflate(R.layout.dialog_coordinates, null))
                .setPositiveButton("enter", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                noticeDialogListener.onDialogPositiveClick(CoordinatesDialog.this);
                            }
                        })
                .setNegativeButton("cancel", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                noticeDialogListener.onDialogNegativeClick(CoordinatesDialog.this);
                            }
                        });
        return dialogBuilder.create();
    }

    @Override
    public void onStart() {
        super.onStart();
        Resources resources = getResources();
        Button positive_button =  ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_POSITIVE);
        positive_button.setBackground(resources.getDrawable(R.drawable.myrect));
        Button negative_button =  ((AlertDialog) getDialog()).getButton(DialogInterface.BUTTON_NEGATIVE);
        negative_button.setBackground(resources.getDrawable(R.drawable.myrect));
    }
}
