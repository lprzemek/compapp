package com.przemekdev.compass;

import android.content.Context;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class LocationClient implements  GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String ERR = "error";

    private Location currentLocation;
    private Location lastKnownLocation;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;

    public LocationClient(Context context){
        googleApiClient = new GoogleApiClient.Builder(context)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    public Location getCurrentLocation(){
        return currentLocation;
    }

    protected void createLocationRequest() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(5000);
        locationRequest.setFastestInterval(2000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    protected void connect(){
        googleApiClient.connect();
    }

    protected void reconnect(){
        googleApiClient.reconnect();
    }

    protected void disconnect(){
        if(googleApiClient.isConnected()) {
            googleApiClient.disconnect();
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        startLocationUpdates();
    }


    protected void startLocationUpdates() {
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnectionSuspended(int i) {
        googleApiClient.connect();
    }

    @Override
    public void onLocationChanged(Location location) {
        currentLocation = location;
    }

    protected float setInitialDestinationBearing(Location destinationLocation) {
        float bearing=0;
        if(lastKnownLocation != null){
            bearing = lastKnownLocation.bearingTo(destinationLocation);
        } else{
            Log.e(ERR, "Can not get last known location");
        }
        return bearing;
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }
}
